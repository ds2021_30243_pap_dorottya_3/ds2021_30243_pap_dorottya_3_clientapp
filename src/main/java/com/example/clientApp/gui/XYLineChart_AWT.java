package com.example.clientApp.gui;

import java.awt.Color;
import java.util.Map;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.ApplicationFrame;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

public class XYLineChart_AWT extends ApplicationFrame {

    public XYLineChart_AWT(String title, Map<String, Map<Integer, Float>> dict) {
        super(title);
        JFreeChart xylineChart = ChartFactory.createXYLineChart(
                title ,
                "Hours" ,
                "Values" ,
                createDataset(dict) ,
                PlotOrientation.VERTICAL ,
                true , true , false);

        ChartPanel chartPanel = new ChartPanel( xylineChart );
        chartPanel.setPreferredSize( new java.awt.Dimension( 800 , 600 ) );
        final XYPlot plot = xylineChart.getXYPlot( );

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
        renderer.setSeriesPaint( 0 , Color.RED );
        renderer.setSeriesPaint( 1 , Color.GREEN );
        renderer.setSeriesPaint( 2 , Color.YELLOW );
        renderer.setSeriesPaint( 3 , Color.GRAY );
        renderer.setSeriesPaint( 4 , Color.black );
        renderer.setSeriesPaint( 5 , Color.blue );
        renderer.setSeriesPaint( 6 , Color.orange );
        renderer.setSeriesPaint( 7 , Color.magenta );
        plot.setRenderer( renderer );
        setContentPane( chartPanel );
    }

    public XYDataset createDataset(Map<String, Map<Integer, Float>> sensorDict) {
        final XYSeriesCollection dataset = new XYSeriesCollection( );

        for (Map.Entry<String, Map<Integer, Float>> day : sensorDict.entrySet()) {
            final XYSeries series = new XYSeries( "Day " + day.getKey() );

            for (Map.Entry<Integer, Float> hour : day.getValue().entrySet()) {
                series.add( hour.getKey() , hour.getValue());
            }

            dataset.addSeries( series );
        }
        return dataset;
    }
}