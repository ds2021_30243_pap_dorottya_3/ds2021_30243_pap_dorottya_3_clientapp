package com.example.clientApp.gui;

import javax.swing.*;
import java.awt.*;

public class ClientGUI {
    private JFrame mainFrame;
    private JLabel lastDaysJLabel;
    private JTextField lastXDaysTextField;
    private JButton getHistoryChartButton;

    private JButton getBaselineButton;
    private JLabel baselineValueLabel;

    private JPanel mainPanel;
    private JPanel historyInputPanel;
    private JPanel baselinePanel;

    public ClientGUI() {
        mainFrame = new JFrame("Measurement APP");
        lastDaysJLabel = new JLabel("Introduce the number of days: ");
        lastXDaysTextField = new JTextField(15);
        getHistoryChartButton = new JButton("Show chart");

        historyInputPanel = new JPanel();
        historyInputPanel.add(lastDaysJLabel);
        historyInputPanel.add(lastXDaysTextField);
        historyInputPanel.add(getHistoryChartButton);
        historyInputPanel.setLayout(new FlowLayout());

        getBaselineButton = new JButton("Get baseline for the last 7 days");
        baselineValueLabel = new JLabel();
        baselinePanel = new JPanel();
        baselinePanel.add(getBaselineButton);
        baselinePanel.add(baselineValueLabel);
        baselinePanel.setLayout(new FlowLayout());

        mainPanel = new JPanel();
        mainPanel.add(historyInputPanel);
        mainPanel.add(baselinePanel);
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        mainFrame.setContentPane(mainPanel);
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(600,200);
    }

    public JButton getHistoryChartButton() {
        return getHistoryChartButton;
    }

    public JTextField getLastXDays() {
        return lastXDaysTextField;
    }

    public JButton getBaselineButton() {
        return getBaselineButton;
    }

    public void setBaslineValue(double value) {
        baselineValueLabel.setText("The baseline value is: " + value);
        baselinePanel.add(baselineValueLabel);
    }
}
