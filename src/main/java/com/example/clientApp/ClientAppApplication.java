package com.example.clientApp;

import com.example.clientApp.entities.HistoryConsumptionValue;
import com.example.clientApp.gui.ClientGUI;
import com.example.clientApp.gui.XYLineChart_AWT;
import com.googlecode.jsonrpc4j.spring.rest.JsonRpcRestClient;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@SpringBootApplication
public class ClientAppApplication {
    @RequestMapping
    @ResponseBody
    public static void func(@RequestParam("a") int a, @RequestParam("b") int b) throws Throwable {
        URL url = new URL("https://backend-ass2.herokuapp.com/externalDevice");
        JsonRpcRestClient client = new JsonRpcRestClient(url);
        Map<String, Object> params = new HashMap<>();
        params.put("a", a);
        params.put("b", b);
        System.out.println(client.invoke("multiplier", params, Object.class));
    }

    @RequestMapping
    @ResponseBody
    public static Map<String, Map<String, Map<Integer, Float>>> getHistoricalConsumption(@RequestParam("id") UUID id, @RequestParam("days") int days) throws Throwable {
        URL url = new URL("https://backend-ass2.herokuapp.com//externalDevice");
        JsonRpcRestClient client = new JsonRpcRestClient(url);
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("days", days);

        List<HistoryConsumptionValue> historyConsumptionValues = Arrays.asList(client.invoke("getHistoricalConsumption", params, HistoryConsumptionValue[].class));

        Map<String, Map<String, Map<Integer, Float>>> dictionary = new HashMap<String, Map<String, Map<Integer, Float>>>();

        // settings ids
        for(HistoryConsumptionValue h: historyConsumptionValues) {
            dictionary.put(h.sensorId.toString(), new HashMap<String, Map<Integer, Float>>());
            //System.out.println(h.sensorId);
        }

        // setting dates
        for (Map.Entry<String, Map<String, Map<Integer, Float>>> sensorId : dictionary.entrySet()) {
            // filtering original list to contain only sensor id values
            List<HistoryConsumptionValue> sensorIdHistoryConsumption = historyConsumptionValues
                    .stream()
                    .filter(p -> p.sensorId.toString().equals(sensorId.getKey()))
                    .collect(Collectors.toList());

            //set the last days and 24 hours
            Timestamp today = new Timestamp(System.currentTimeMillis());
            int todayMonthDay = today.toLocalDateTime().getDayOfMonth();
            int lastActieDays = todayMonthDay - days;

            //System.out.println(lastActieDays);

            for(int i = lastActieDays + 1; i <= todayMonthDay; i++) {
                Map<Integer, Float> hoursDictionary = new HashMap<Integer, Float>();

                for(int j = 0; j < 24; j++) {
                    hoursDictionary.put(j, 0.0f);
                }

                sensorId.getValue().put(Integer.toString(i), hoursDictionary);
            }

            // summing values by hours
            for(HistoryConsumptionValue historyConsumptionValue: sensorIdHistoryConsumption) {
                String date = historyConsumptionValue.timeStamp.toString();
                String day = date.substring(8, 10).replaceAll("^0+(?!$)", "");
                Integer hour = Integer.parseInt(date.substring(11, 13).replaceAll("^0+(?!$)", ""));

//                 System.out.println(date +"     " + day+ "    " + hour);
                Float hourSum =  sensorId.getValue().get(day).get(hour);
                sensorId.getValue().get(day).put(hour, hourSum + historyConsumptionValue.value);
            }
        }

        //print
        for (Map.Entry<String, Map<String, Map<Integer, Float>>> sensorId : dictionary.entrySet()) {
            for (Map.Entry<String, Map<Integer, Float>> day : sensorId.getValue().entrySet()) {
                for (Map.Entry<Integer, Float> hour : day.getValue().entrySet()) {
                    System.out.println(sensorId.getKey() + " "
                    + day.getKey() + " "
                    + hour.getKey() + " - " + hour.getValue());
                }
            }
        }

        return dictionary;
    }

    @RequestMapping
    @ResponseBody
    public static double getAverageConsumptionForLast7Days(@RequestParam("id") UUID id) throws Throwable {
        //URL url = new URL("http://localhost:8080/externalDevice");
        URL url = new URL("https://backend-ass2.herokuapp.com/externalDevice");
        JsonRpcRestClient client = new JsonRpcRestClient(url);
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);

        //System.out.println(client.invoke("getAverageConsumptionForLast7Days", params, Object.class));
        return (double) client.invoke("getAverageConsumptionForLast7Days", params, Object.class);
    }

    public static void main(String[] args) throws Throwable {
        //SpringApplication.run(ClientAppApplication.class, args);
        //func(4, 5);

        UUID clientId = UUID.fromString("0c99e324-40db-4611-8309-94a78c89fec4");

        ClientGUI clientGUI = new ClientGUI();

        clientGUI.getHistoryChartButton().addActionListener(e -> {
            // get user input
            int lastDays;
            if(clientGUI.getLastXDays().getText().isEmpty()) {
                lastDays = 7;
            }
            else {
                lastDays = Integer.parseInt(clientGUI.getLastXDays().getText());
            }

            try {
                Map<String, Map<String, Map<Integer, Float>>> dict = getHistoricalConsumption(clientId, lastDays);

                for (Map.Entry<String, Map<String, Map<Integer, Float>>> sensorId : dict.entrySet()) {
                    XYLineChart_AWT chartGUI = new XYLineChart_AWT(sensorId.getKey(), sensorId.getValue());
                    chartGUI.pack();
                    RefineryUtilities.centerFrameOnScreen(chartGUI);
                    chartGUI.setVisible( true );
                }
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        });

        clientGUI.getBaselineButton().addActionListener(e -> {
            try {
                double baslineValue = getAverageConsumptionForLast7Days(clientId);

                clientGUI.setBaslineValue(baslineValue);
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        });
    }

}
