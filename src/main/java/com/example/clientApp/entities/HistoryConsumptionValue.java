package com.example.clientApp.entities;

import java.sql.Timestamp;
import java.util.UUID;

public class HistoryConsumptionValue {
    public UUID sensorId;
    public Timestamp timeStamp;
    public float value;
}
